<?php

class MakeupHtmlTest extends \PHPUnit_Framework_TestCase
{
    public function testImg()
    {
        $html = 'Here is my image: http://www.noop.lv/wp-content/themes/twentyeleven/images/headers/wheel.jpg';

        $this->assertEquals('Here is my image: <img src="http://www.noop.lv/wp-content/themes/twentyeleven/images/headers/wheel.jpg" width="100" height="28"/>',
            $this->wipeHtml($html));
    }

    public function testUrl()
    {
        $html = '<div onmouseover="someNastyThing()">Hi!</div> Please check this atricle: http://rus.delfi.lv/news/daily/politics/razhuks-nuzhen-zakon-o-veteranah-pora-prekratit-vojnu.d?id=42495020 meow? http://noop.lv/?action=boo&user=Жопа.';

        $this->assertEquals('Hi! Please check this atricle: <a href="http://rus.delfi.lv/news/daily/politics/razhuks-nuzhen-zakon-o-veteranah-pora-prekratit-vojnu.d?id=42495020" target="_blank" rel="nofollow">rus.delfi.lv/news/daily/politics/razh...</a> meow? <a href="http://noop.lv/?action=boo&user=Жопа" target="_blank" rel="nofollow">noop.lv/?action=boo&user=Жопа</a>.',
        $this->wipeHtml($html));
    }

    public function testImages()
    {
        foreach(array(
                    'http://d24w6bsrhbeh9d.cloudfront.net/photo/4724243_700b.jpg' => '<img src="http://d24w6bsrhbeh9d.cloudfront.net/photo/4724243_700b.jpg" width="100" height="206"/>',
                    'http://3.bp.blogspot.com/-5OfQy7lYM-I/TbCU7Z5vSlI/AAAAAAAABTU/vIpjd__3vJE/s1600/very_fluffy_bunny1.jpg' => '<img src="http://3.bp.blogspot.com/-5OfQy7lYM-I/TbCU7Z5vSlI/AAAAAAAABTU/vIpjd__3vJE/s1600/very_fluffy_bunny1.jpg" width="100" height="76"/>',
                    'http://upload.wikimedia.org/wikipedia/commons/8/88/Fluffy_white_bunny_rabbit.jpg' => '<img src="http://upload.wikimedia.org/wikipedia/commons/8/88/Fluffy_white_bunny_rabbit.jpg" width="100" height="117"/>',
                    'http://images2.wikia.nocookie.net/__cb20090216235741/uncyclopedia/images/1/12/Fluffy.jpg' => '<img src="http://images2.wikia.nocookie.net/__cb20090216235741/uncyclopedia/images/1/12/Fluffy.jpg" width="100" height="106"/>',
                    'http://cdn2.mixrmedia.com/wp-uploads/girlybubble/blog/2011/04/fluffy-bunny-baby-bunny-3518631-500-366.jpg' => '<img src="http://cdn2.mixrmedia.com/wp-uploads/girlybubble/blog/2011/04/fluffy-bunny-baby-bunny-3518631-500-366.jpg" width="100" height="73"/>',
                    'http://www.hedweb.com/fluffy.jpg' => '<img src="http://www.hedweb.com/fluffy.jpg" width="100" height="123"/>',
                    'http://i.chzbgr.com/completestore/2009/2/4/128782715113151391.jpg' => '<img src="http://i.chzbgr.com/completestore/2009/2/4/128782715113151391.jpg" width="100" height="75"/>',
                    'http://fc01.deviantart.net/fs9/i/2006/144/8/f/Little_fluffy_cloud_by_petelea.jpg' => '<img src="http://fc01.deviantart.net/fs9/i/2006/144/8/f/Little_fluffy_cloud_by_petelea.jpg" width="100" height="75"/>',
                    'http://th01.deviantart.net/fs30/PRE/i/2009/246/2/f/Fluffy_Bumble_bee_02_by_Sirusdark.jpg' => '<img src="http://th01.deviantart.net/fs30/PRE/i/2009/246/2/f/Fluffy_Bumble_bee_02_by_Sirusdark.jpg" width="100" height="66"/>',
                    'http://wallpaperswide.com/thumbs/fluffy_kittens_2-t2.jpg' => '<img src="http://wallpaperswide.com/thumbs/fluffy_kittens_2-t2.jpg" width="100" height="64"/>'
            ) as $image => $expected) {
            $this->assertEquals($expected, $this->wipeHtml($image));
        }
    }

    public function testFullText()
    {
$text = <<<TEXT
Hi! My name is sandy, my blog is http://noop.lv/, and I also put some pictures on http://some-very-unknown-domain.com.com.com/text?.

http://d24w6bsrhbeh9d.cloudfront.net/photo/4724243_700b.jpg
http://3.bp.blogspot.com/-5OfQy7lYM-I/TbCU7Z5vSlI/AAAAAAAABTU/vIpjd__3vJE/s1600/very_fluffy_bunny1.jpg
http://upload.wikimedia.org/wikipedia/commons/8/88/Fluffy_white_bunny_rabbit.jpg
http://images2.wikia.nocookie.net/__cb20090216235741/uncyclopedia/images/1/12/Fluffy.jpg
http://cdn2.mixrmedia.com/wp-uploads/girlybubble/blog/2011/04/fluffy-bunny-baby-bunny-3518631-500-366.jpg
http://www.hedweb.com/fluffy.jpg
http://i.chzbgr.com/completestore/2009/2/4/128782715113151391.jpg
http://fc01.deviantart.net/fs9/i/2006/144/8/f/Little_fluffy_cloud_by_petelea.jpg

check also these fluffy videos!

http://youtu.be/HECa3bAFAYk?feature=boo#foobar
http://youtu.be/598IdFlOXcQ
TEXT;
$expect = <<<EXPECT
Hi! My name is sandy, my blog is <a href="http://noop.lv/" target="_blank" rel="nofollow">noop.lv/</a>, and I also put some pictures on <a href="http://some-very-unknown-domain.com.com.com/text?" target="_blank" rel="nofollow">some-very-unknown-domain.com.com.com/...</a>.

<img src="http://d24w6bsrhbeh9d.cloudfront.net/photo/4724243_700b.jpg" width="100" height="206"/>
<img src="http://3.bp.blogspot.com/-5OfQy7lYM-I/TbCU7Z5vSlI/AAAAAAAABTU/vIpjd__3vJE/s1600/very_fluffy_bunny1.jpg" width="100" height="76"/>
<img src="http://upload.wikimedia.org/wikipedia/commons/8/88/Fluffy_white_bunny_rabbit.jpg" width="100" height="117"/>
<a href="http://images2.wikia.nocookie.net/__cb20090216235741/uncyclopedia/images/1/12/Fluffy.jpg" target="_blank" rel="nofollow">images2.wikia.nocookie.net/__cb200902...</a>
<a href="http://cdn2.mixrmedia.com/wp-uploads/girlybubble/blog/2011/04/fluffy-bunny-baby-bunny-3518631-500-366.jpg" target="_blank" rel="nofollow">cdn2.mixrmedia.com/wp-uploads/girlybu...</a>
<a href="http://www.hedweb.com/fluffy.jpg" target="_blank" rel="nofollow">www.hedweb.com/fluffy.jpg</a>
<a href="http://i.chzbgr.com/completestore/2009/2/4/128782715113151391.jpg" target="_blank" rel="nofollow">i.chzbgr.com/completestore/2009/2/4/1...</a>
<a href="http://fc01.deviantart.net/fs9/i/2006/144/8/f/Little_fluffy_cloud_by_petelea.jpg" target="_blank" rel="nofollow">fc01.deviantart.net/fs9/i/2006/144/8/...</a>

check also these fluffy videos!

<iframe width="960" height="75" src="http://www.youtube.com/embed/HECa3bAFAYk" frameborder="0" allowfullscreen></iframe>
<iframe width="960" height="75" src="http://www.youtube.com/embed/598IdFlOXcQ" frameborder="0" allowfullscreen></iframe>
EXPECT;

        $this->assertEquals($expect,
            $this->wipeHtml(
                $text
            ));
    }

    public function testTimeout()
    {
        $this->assertEquals('<a href="http://asdfasdfasdfsafasfasfdfasdfdasf.com/foobar" target="_blank" rel="nofollow">asdfasdfasdfsafasfasfdfasdfdasf.com/f...</a>',
            $this->wipeHtml('http://asdfasdfasdfsafasfasfdfasdfdasf.com/foobar'));
    }

    /**
     * Wipes html
     * @param $html html to process
     * @param int $maxWidth max width for images detected
     * @param int $maxLetters max letters inside link <a href="">maxLetter</a>
     * @param bool $fixTrailing fix trailing characters like http://google.com. (trailing dot)
     * @param int $maxDataLength max data length to use for checking images / image sizes
     * @param int $maxImages max images to process
     * @param int $maxVideos max videos to embed
     * @return string
     */
    protected function wipeHtml($html, $maxWidth = 100, $maxLetters = 40, $fixTrailing = true, $maxDataLength = 1000000, $maxImages = 5, $maxVideos = 5)
    {
        $pattern = '/(http|https):(?:\/\/(?:(?:%[0-9a-f][0-9a-f]|[-a-z0-9\._~\x{A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}\x{10000}-\x{1FFFD}\x{20000}-\x{2FFFD}\x{30000}-\x{3FFFD}\x{40000}-\x{4FFFD}\x{50000}-\x{5FFFD}\x{60000}-\x{6FFFD}\x{70000}-\x{7FFFD}\x{80000}-\x{8FFFD}\x{90000}-\x{9FFFD}\x{A0000}-\x{AFFFD}\x{B0000}-\x{BFFFD}\x{C0000}-\x{CFFFD}\x{D0000}-\x{DFFFD}\x{E1000}-\x{EFFFD}!\$&\'\(\)\*\+,;=:])*@)?(?:\[(?:(?:(?:[0-9a-f]{1,4}:){6}(?:[0-9a-f]{1,4}:[0-9a-f]{1,4}|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(?:\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3})|::(?:[0-9a-f]{1,4}:){5}(?:[0-9a-f]{1,4}:[0-9a-f]{1,4}|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(?:\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3})|(?:[0-9a-f]{1,4})?::(?:[0-9a-f]{1,4}:){4}(?:[0-9a-f]{1,4}:[0-9a-f]{1,4}|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(?:\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3})|(?:[0-9a-f]{1,4}:[0-9a-f]{1,4})?::(?:[0-9a-f]{1,4}:){3}(?:[0-9a-f]{1,4}:[0-9a-f]{1,4}|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(?:\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3})|(?:(?:[0-9a-f]{1,4}:){0,2}[0-9a-f]{1,4})?::(?:[0-9a-f]{1,4}:){2}(?:[0-9a-f]{1,4}:[0-9a-f]{1,4}|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(?:\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3})|(?:(?:[0-9a-f]{1,4}:){0,3}[0-9a-f]{1,4})?::[0-9a-f]{1,4}:(?:[0-9a-f]{1,4}:[0-9a-f]{1,4}|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(?:\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3})|(?:(?:[0-9a-f]{1,4}:){0,4}[0-9a-f]{1,4})?::(?:[0-9a-f]{1,4}:[0-9a-f]{1,4}|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(?:\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3})|(?:(?:[0-9a-f]{1,4}:){0,5}[0-9a-f]{1,4})?::[0-9a-f]{1,4}|(?:(?:[0-9a-f]{1,4}:){0,6}[0-9a-f]{1,4})?::)|v[0-9a-f]+[-a-z0-9\._~!\$&\'\(\)\*\+,;=:]+)\]|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(?:\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}|(?:%[0-9a-f][0-9a-f]|[-a-z0-9\._~\x{A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}\x{10000}-\x{1FFFD}\x{20000}-\x{2FFFD}\x{30000}-\x{3FFFD}\x{40000}-\x{4FFFD}\x{50000}-\x{5FFFD}\x{60000}-\x{6FFFD}\x{70000}-\x{7FFFD}\x{80000}-\x{8FFFD}\x{90000}-\x{9FFFD}\x{A0000}-\x{AFFFD}\x{B0000}-\x{BFFFD}\x{C0000}-\x{CFFFD}\x{D0000}-\x{DFFFD}\x{E1000}-\x{EFFFD}!\$&\'\(\)\*\+,;=@])*)(?::[0-9]*)?(?:\/(?:(?:%[0-9a-f][0-9a-f]|[-a-z0-9\._~\x{A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}\x{10000}-\x{1FFFD}\x{20000}-\x{2FFFD}\x{30000}-\x{3FFFD}\x{40000}-\x{4FFFD}\x{50000}-\x{5FFFD}\x{60000}-\x{6FFFD}\x{70000}-\x{7FFFD}\x{80000}-\x{8FFFD}\x{90000}-\x{9FFFD}\x{A0000}-\x{AFFFD}\x{B0000}-\x{BFFFD}\x{C0000}-\x{CFFFD}\x{D0000}-\x{DFFFD}\x{E1000}-\x{EFFFD}!\$&\'\(\)\*\+,;=:@]))*)*|\/(?:(?:(?:(?:%[0-9a-f][0-9a-f]|[-a-z0-9\._~\x{A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}\x{10000}-\x{1FFFD}\x{20000}-\x{2FFFD}\x{30000}-\x{3FFFD}\x{40000}-\x{4FFFD}\x{50000}-\x{5FFFD}\x{60000}-\x{6FFFD}\x{70000}-\x{7FFFD}\x{80000}-\x{8FFFD}\x{90000}-\x{9FFFD}\x{A0000}-\x{AFFFD}\x{B0000}-\x{BFFFD}\x{C0000}-\x{CFFFD}\x{D0000}-\x{DFFFD}\x{E1000}-\x{EFFFD}!\$&\'\(\)\*\+,;=:@]))+)(?:\/(?:(?:%[0-9a-f][0-9a-f]|[-a-z0-9\._~\x{A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}\x{10000}-\x{1FFFD}\x{20000}-\x{2FFFD}\x{30000}-\x{3FFFD}\x{40000}-\x{4FFFD}\x{50000}-\x{5FFFD}\x{60000}-\x{6FFFD}\x{70000}-\x{7FFFD}\x{80000}-\x{8FFFD}\x{90000}-\x{9FFFD}\x{A0000}-\x{AFFFD}\x{B0000}-\x{BFFFD}\x{C0000}-\x{CFFFD}\x{D0000}-\x{DFFFD}\x{E1000}-\x{EFFFD}!\$&\'\(\)\*\+,;=:@]))*)*)?|(?:(?:(?:%[0-9a-f][0-9a-f]|[-a-z0-9\._~\x{A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}\x{10000}-\x{1FFFD}\x{20000}-\x{2FFFD}\x{30000}-\x{3FFFD}\x{40000}-\x{4FFFD}\x{50000}-\x{5FFFD}\x{60000}-\x{6FFFD}\x{70000}-\x{7FFFD}\x{80000}-\x{8FFFD}\x{90000}-\x{9FFFD}\x{A0000}-\x{AFFFD}\x{B0000}-\x{BFFFD}\x{C0000}-\x{CFFFD}\x{D0000}-\x{DFFFD}\x{E1000}-\x{EFFFD}!\$&\'\(\)\*\+,;=:@]))+)(?:\/(?:(?:%[0-9a-f][0-9a-f]|[-a-z0-9\._~\x{A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}\x{10000}-\x{1FFFD}\x{20000}-\x{2FFFD}\x{30000}-\x{3FFFD}\x{40000}-\x{4FFFD}\x{50000}-\x{5FFFD}\x{60000}-\x{6FFFD}\x{70000}-\x{7FFFD}\x{80000}-\x{8FFFD}\x{90000}-\x{9FFFD}\x{A0000}-\x{AFFFD}\x{B0000}-\x{BFFFD}\x{C0000}-\x{CFFFD}\x{D0000}-\x{DFFFD}\x{E1000}-\x{EFFFD}!\$&\'\(\)\*\+,;=:@]))*)*|(?!(?:%[0-9a-f][0-9a-f]|[-a-z0-9\._~\x{A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}\x{10000}-\x{1FFFD}\x{20000}-\x{2FFFD}\x{30000}-\x{3FFFD}\x{40000}-\x{4FFFD}\x{50000}-\x{5FFFD}\x{60000}-\x{6FFFD}\x{70000}-\x{7FFFD}\x{80000}-\x{8FFFD}\x{90000}-\x{9FFFD}\x{A0000}-\x{AFFFD}\x{B0000}-\x{BFFFD}\x{C0000}-\x{CFFFD}\x{D0000}-\x{DFFFD}\x{E1000}-\x{EFFFD}!\$&\'\(\)\*\+,;=:@])))(?:\?(?:(?:%[0-9a-f][0-9a-f]|[-a-z0-9\._~\x{A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}\x{10000}-\x{1FFFD}\x{20000}-\x{2FFFD}\x{30000}-\x{3FFFD}\x{40000}-\x{4FFFD}\x{50000}-\x{5FFFD}\x{60000}-\x{6FFFD}\x{70000}-\x{7FFFD}\x{80000}-\x{8FFFD}\x{90000}-\x{9FFFD}\x{A0000}-\x{AFFFD}\x{B0000}-\x{BFFFD}\x{C0000}-\x{CFFFD}\x{D0000}-\x{DFFFD}\x{E1000}-\x{EFFFD}!\$&\'\(\)\*\+,;=:@])|[\x{E000}-\x{F8FF}\x{F0000}-\x{FFFFD}|\x{100000}-\x{10FFFD}\/\?])*)?(?:\#(?:(?:%[0-9a-f][0-9a-f]|[-a-z0-9\._~\x{A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}\x{10000}-\x{1FFFD}\x{20000}-\x{2FFFD}\x{30000}-\x{3FFFD}\x{40000}-\x{4FFFD}\x{50000}-\x{5FFFD}\x{60000}-\x{6FFFD}\x{70000}-\x{7FFFD}\x{80000}-\x{8FFFD}\x{90000}-\x{9FFFD}\x{A0000}-\x{AFFFD}\x{B0000}-\x{BFFFD}\x{C0000}-\x{CFFFD}\x{D0000}-\x{DFFFD}\x{E1000}-\x{EFFFD}!\$&\'\(\)\*\+,;=:@])|[\/\?])*)?/iu';

        $html = strip_tags($html);

        preg_match_all($pattern, $html, $matches);

        $replaces = array();

        if (is_array($matches) &&
            isset($matches[0])) {
            //init limits
            $processedImages = 0;
            $processedVideos = 0;

            foreach ($matches[0] as $originalUrl) {
                $trailingChar = '';

                //fix trailing dots/chars
                if ($fixTrailing && in_array(substr($originalUrl, -1), array('.', ',', ';', ':'))) {
                    $url = substr($originalUrl, 0, strlen($originalUrl) - 1);
                    $trailingChar = substr($originalUrl, - 1);
                } else {
                    $url = $originalUrl;
                }

                //video processing
                if ($processedVideos < $maxVideos) {
                    if (preg_match('/^http:\/\/youtu\.be\/([A-Z0-9-_]+)([^\s]*)$/i', $url)) {
                        $replaces[$originalUrl] = preg_replace('/http:\/\/youtu\.be\/([A-Z0-9-_]+)([^\s]*)/i', '<iframe width="960" height="75" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>', $url);
                        $processedVideos ++;
                        continue;
                    }
                }




                //is this and image?
                if ($processedImages < $maxImages) {
                    $processedImages++;
                    $isImage = true;

                    $context = stream_context_create(array('http' => array(
                        'timeout' => 0.5
                        ),
                        'https' => array(
                            'timeout' => 0.5
                        )));

                    $data = @file_get_contents($url, false, $context, 0, $maxDataLength);

                    $im = new \Imagick();

                    try
                    {
                        $im->readImageBlob($data);
                    } catch(\ImagickException $e) {
                        $isImage = false;
                    }

                    if ($isImage) {
                        //sizes
                        $width = $im->getImageWidth();
                        $height = $im->getImageHeight();

                        if ($width > $maxWidth) {
                            $height = floor($height * $maxWidth / $width);
                            $width = $maxWidth;
                        }

                        //work with image
                        $replaces[$originalUrl] = '<img src="' . $url . '" width="' . $width . '" height="' . $height . '"/>';

                        unset($im);
                        continue;
                    }

                    //cleanup anyway
                    unset($im);
                    unset($context);
                    unset($data);
                }

                //general replace
                $urlTxt = str_replace(array('http://', 'https://'), '', $url);

                if (strlen($urlTxt) > $maxLetters) {
                    $urlTxt = substr($urlTxt, 0, $maxLetters - 3) . '...';
                }

                $replaces[$originalUrl] = '<a href="' . $url . '" target="_blank" rel="nofollow">' . $urlTxt . '</a>' . $trailingChar;
            }
        }

        $html = strtr($html, $replaces);

        return $html;
    }
}